<?php

/**
 * @author Lewis Kimani <kimanilewi@gmail.com>
 */

namespace Argila\ArgilaCoreAPI\Controllers;

use Argila\ArgilaCoreAPI\Config\StatusCodes;
use Argila\ArgilaCoreAPI\Models\User as Request;
use Argila\ArgilaCoreAPI\Models\customerAccounts as customerAccounts;
use Argila\ArgilaCoreAPI\Utilities\SyncLogger as logger;
use Symfony\Component\Config\Definition\Exception\Exception;
use Ubench as benchmark;
use Argila\ArgilaCoreAPI\Config\Config;
use Argila\ArgilaCoreAPI\Config\Validation;
use Argila\ArgilaCoreAPI\Utilities\Helpers;
use Argila\ArgilaCoreAPI\Utilities\CoreUtils as CoreUtils;

/**
 * Card Registration Controller
 */
class CardRegistrationController {

    /**
     * * log class
     * */
    private $log;

    /**
     * benchmark class.
     */
    private $benchmark;
    private $coreUtils;
    private $helpers;
    private $clientID;

    function __construct() {
        $this->log = new logger();
        $this->benchmark = new benchmark();
        $this->coreUtils = new CoreUtils();
        $this->helpers = new helpers();
    }

    function CardRequest($request) {

        $this->benchmark->start();
        $validator = new Validation();
        $results = array();
        $this->log->info(Config::info, -1,
                "Received cards request "
                . $this->log->printArray($request));
        /**
         * *********************************************************************
         * Get request function and forward request for processing
         */
        $this->log->debug(Config::debug, -1,
                "Received cards data...."
                . $this->log->printArray($request));
        /**
         * check client.
         */
        $clientID = 0;
        $clientDetails = $this->coreUtils->getClient($request['client']['clientCode']);
        $this->log->debug(Config::debug, -1,
                "Client already exists. Details...."
                . $this->log->printArray($clientDetails));
        if (is_array($clientDetails) && count($clientDetails) > 0) {
            $this->log->debug(Config::debug, -1,
                    "Client already exists. Details...."
                    . $this->log->printArray($clientDetails));
            $clientID = $clientDetails['clientID'];
        }
        else {
            /**
             * create client;
             */
            $this->log->debug(Config::debug, -1,
                    "Client to be added..." . $this->log->printArray($value));
            $rules = $validator->rules['client'];

            try {
                if ($this->helpers->validateParams($request['client'], $rules)) {

                    $clientCode = $request['client']['clientCode'];
                    $clientName = $request['client']['clientName'];
                    $clientID = $this->coreUtils->createClient($clientCode,
                            $clientName);
                    $this->log->debug(Config::debug, -1,
                            "client added..." . $this->log->printArray($clientID));
                }
            } catch (\Exception $ex) {
                $this->log->error(Config::error, -1,
                        "An Error Occurred" . $ex->getMessage());
            }
        }
        $locationDetails = $this->coreUtils->getLocation($request['location']['location_id']);

        if (isset($accountData['locationName'])) {
            $this->log->debug(Config::debug, -1, "location already exists...");
            $accountData['locationName'] = $locationDetails['locationName'];
            $locationID = $locationDetails['locationID'];
        }
        else {
            foreach ($request['locations'] as $key => $value) {
                $this->log->debug(Config::debug, -1,
                        "location to be added..." . $this->log->printArray($value));
                $rules = $validator->rules['locations'];
                try {
                    if ($this->helpers->validateParams($value, $rules)) {
                        $location = $value['locationName'];
                        $venueID = $value['vanue_id'];
                        $advert = $value['advert'];
                        $token = $this->coreUtils->randomCode(16);
                        $response = $this->coreUtils->createLocation($clientID,
                                $venueID, $location, $token, $advert);
                        $this->log->debug(Config::debug, -1,
                                "location added..." . $this->log->printArray($response));
                    }
                } catch (\Exception $ex) {
                    $this->log->error(Config::error, -1,
                            "An Error Occurred" . $ex->getMessage());
                }
            }
            die();


            /**
             * *********************************************************************
             * Check if card  exists
             */
            return Config::FAILED_RESPONSE;
        }
    }

    public function addCards($request) {
        $this->benchmark->start();
        $validator = new Validation();
        $results = array();
        $this->log->info(Config::info, -1,
                "Received cards request "
                . $this->log->printArray($request));
        /**
         * *********************************************************************
         * Get request function and forward request for processing
         */
        $this->log->debug(Config::debug, -1,
                "Received cards data...."
                . $this->log->printArray($request));
        $venueID = $request['location']['location_id'];
        $this->log->debug(Config::debug, -1, "Venue...." . $venueID);
        $locationDetails = $this->coreUtils->getLocation($venueID);
        $this->log->debug(Config::debug, -1,
                "location already exists..."
                . $this->log->printArray($locationDetails));
        if (is_array($locationDetails)) {
            $this->log->debug(Config::debug, -1, "location already exists...");
            $accountData['locationName'] = $locationDetails['locationName'];
            $locationID = $locationDetails['locationID'];

            foreach ($request['cards'] as $value) {
                $accountData = $this->coreUtils->checkAccountProfile($value);
                if (empty($accountData)) {
                    $this->log->debug(Config::debug, -1,
                            "card to be added..." . $this->log->printArray($value));
                    try {
                        $response = $this->coreUtils->createCard($locationID,
                                $value);
                        $this->log->debug(Config::debug, -1,
                                "card added..." . $value);
                    } catch (\Exception $ex) {
                        $this->log->error(Config::error, -1,
                                "An Error Occurred" . $ex->getMessage());
                    }
                }
                else {
                    /**
                     * card exists
                     */
                    $this->log->error(Config::error, -1, "Card already exists");
                }
            }
        }
    }

}
