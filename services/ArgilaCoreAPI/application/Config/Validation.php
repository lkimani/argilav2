<?php

Namespace Argila\ArgilaCoreAPI\Config;

/**
 * * validation rules 
 * * loads all the validation rules
 * * @author kimanilewi@gmail.com
 * * (should be in a model ideally )
 * */
class Validation {

    public $rules = array(
        'mpesa_request' => array(
            'required' => array(
                'BillRefNumber',
                'TransID',
                'TransAmount',
                'TransTime',
                'FirstName',
                'MSISDN'
            ),
            'string'   => array(
                'BillRefNumber',
                'TransType',
                'FirstName',
                'MiddleName',
            ),
            'double'   => array(
                'TransAmount'
            )
        ),
        'pos'           => array(
            'required' => array(
                'accountNumber',
                'location_id',
            ),
            'int'      => array(
                'batteryLevel'
            ),
        ),
        'locations'     => array(
            'required' => array(
                'vanue_id',
                'locationName',
            ),
            'string'   => array(
                'locationName',
                'advert',
                'vanue_id'
            ),
        ),
        'client'        => array(
            'required' => array(
                'clientCode',
                'clientName'
            ),
            'string'   => array(
                'clientCode',
                'clientName'
            ),
        ),
        'card'          => array(
            'required' => array(
                'accountNumber'
            ),
            'string'   => array(
                'accountNumber'
            ),
        )
    );

}
