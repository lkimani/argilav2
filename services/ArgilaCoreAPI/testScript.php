<?php

/**
 * Sample script to test creating;
 *  client
 *  locations 
 *  and adding cards. 
 * 
 * @auther lewis .
 * 
 * - run the script on terminal or via IDE of choice. 
 * - naviagate to the folder where this file is located.
 * - type "php testScript.php"
 */
/**
 * uncomment the endpoint you want to test;
 */
//$apiUrl = "http://40.76.8.133:9000/argilaCore/index.php/locations";
//$apiUrl = "http://40.76.8.133:9000/argilaCore/index.php/accounts";
//$apiUrl = "http://40.76.8.133:9000/argilaCore/index.php/pos";

    $apiUrl = "http://localhost/argilaCore/index.php/mpesa_request";


/**
 * encrypted POS request.
 */
$posRequest = 'eyJjcmVkZW50aWFscyI6eyJUb2tlbiI6ImJpZ3NxdWFyIiwibG9jYXRpb25faWQiOiIxMjNRV0UifSwicGF5bG9hZCI6eyJhY2NvdW50TnVtYmVyIjoiOTIwQjY1MTMiLCJiYXR0ZXJ5TGV2ZWwiOiIwMCIsInNvdXJjZSI6InBvcyJ9fQ==';
/**
 * Location request sample.
 *  - creates a client with one or multiple locations 
 */
$name = "ts" . rand(1000, 999999);
$clientName = "ArgilaHQ" ;
$venueID1 = "TEST" . rand(100, 999);
$venueID2 = "TEST" . rand(100, 999);
$client = '{
        "credentials":{
                 "Token":"bigsquare998874",
                 "location_id":"1234ABCB"},
         "client":{
                 "clientCode":"ARGILA",
                 "clientName":"' . $clientName . '"
                 },
          "locations":[{
                 "vanue_id":"' . $venueID1 . '",
                 "locationName":"ARGILA",
                 "advert":"Mungai testing"
                 },
                 {
                  "vanue_id":"' . $venueID2 . '",
                 "locationName":"java the hub",
                 "advert":"free pizza today"
                 }]
        }';
/**
 * Adds cards to a location. 
 *  - supports both single and batch cards creation which are comma separated. 
 */
$account1 = "B6676D44" ;
$account2 = "" . rand(1000, 9999);
$addCards = '{
        "credentials":{
                 "Token":"ArgilaHQ9",
                 "location_id":"1234ABCB"},
         "location":{
                 "location_id":"Array826"
                 },
          "cards":[
                "' . $account1 . '",
                "' . $account2 . '"
                 ]
        }';
/**
 * USAGE. 
 * $response = post($apiUrl, *enter the payload name you with to test*);
 */
$response = post($apiUrl, $addCards);

print_r($response);

function post($url = "", $fields = array()) {

    global $username, $password;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
  //  curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
//    curl_setopt($ch, CURLOPT_POSTFIELDS, (json_encode($fields)));
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}
